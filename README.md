# Images with Bundled .NET Runtimes

Docker images of bundled .NET runtimes from the official runtime images.

Supports multiple Linux platforms.


## Image Contents
Based on the latest official .NET runtime images from the
[Microsoft Container Registry (MCR)][mcr].

Takes the latest official `runtime:x.y-bullseye-slim` SDK image and bundles
additional runtimes from older supported and LTS runtime releases.


## Usage
Refer to the following registry when requesting an image:
```
registry.gitlab.com/xplo-re/docker/dotnet/runtimes
```


## Tags
Images for .NET runtime versions `5.0`, `6.0` and `7.0` are provided:
- **[Full list of all available tags][taglist]**

Tags are created as follows:
- `5.0`(`-#.#`)\*, `6.0`(`-#.#`)\*, `7.0`(`-#.#`)\*, `latest` \
  Major and minor version numbers of bundled .NET runtime versions in descending
  order (with alias `latest` for the latest .NET runtime version).
- `#.#.#`(`_`...)?(`-#.#.#`(`_`...)?)\* \
  Full version numbers of bundled .NET runtime versions in descending order
  (including any preview/RC version components).


## Platforms and OS
The SDK images are based on Debian Linux 11 "Bullseye".

Supported platforms:
- `linux/amd64`
- `linux/arm64/v8`
- `linux/arm/v7`


## Release Schedule
Images are built on a weekly schedule.

[mcr]: https://mcr.microsoft.com/dotnet
[taglist]: https://gitlab.com/xplo-re/docker/dotnet/runtimes/container_registry