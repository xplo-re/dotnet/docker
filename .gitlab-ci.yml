stages:
  - prepare
  - build
  - test
  - push

variables:
  # The target platforms; must be supported by all .NET runtimes. See Buildx
  # bootstrap output for the list of platform names that can be build on the
  # GitLab node.
  PLATFORMS: linux/amd64,linux/arm64/v8,linux/arm/v7
  # The Docker image tag for this pipeline and definite build target. Tag is
  # based on branch name; do not use slug as it replaces dots by underscores and
  # branch names are usually named after a legacy version.
  PIPELINE_IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME

.docker:
  image: registry.gitlab.com/xplo-re/docker/docker-buildx:20
  services:
    - docker:20-dind
  tags:
    - docker:dind
  variables:
    # https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#tls-enabled
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_CERTDIR: "/certs"
  before_script:
    # Fail if command before pipe fails.
    - set -o pipefail
    # Log in to GitLab container registry.
    # https://docs.gitlab.com/ee/user/packages/container_registry/
    # https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
    - echo -n "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin "$CI_REGISTRY"

# Build a local image and collect version information on the installed frameworks.
# This is later used for tagging multi-platform images.
docker-prepare:
  extends: .docker
  stage: prepare
  script:
    # Get latest image first (if one exists) for caching.
    - docker pull "$CI_REGISTRY_IMAGE:latest" || true
    # Build local image.
    - docker build --cache-from "$CI_REGISTRY_IMAGE:latest"
                   --pull
                   --tag "local-build"
                   .
    # Collect information on .NET runtimes from local image.
    - docker run "local-build"
                 dotnet --info | tee info.txt
    - docker run --mount type=bind,source="$(pwd)",target=/job,readonly
                 "local-build"
                 /job/dotnet-versiontag.sh | tee version.txt
    # Compute short version. Will be empty if any runtime is a pre-release.
    - ./docker-shortversiontag.sh | tee version-short.txt
  artifacts:
    name: dotnet-info
    paths:
      - info.txt
      - version.txt
      - version-short.txt

# Build multi-platform image.
docker-build:
  extends: .docker
  stage: build
  script:
    # Get latest image first (if one exists) for caching.
    - docker pull "$CI_REGISTRY_IMAGE:latest" || true
    # Create builder with own TLS context. Necessary, as the default driver does
    # not support multiple platforms.
    - docker context create tls
    - docker context ls
    - docker buildx create --name multiarch-builder --use tls
    # Collect information on Buildx setup for debugging.
    - docker buildx inspect --bootstrap
    # Build and push images for all supported platforms. Buildx automatically
    # creates manifests such that all platforms are available under all target
    # image tags. Use fix title as GitLab does not allow projects to start with "."
    - docker buildx build --pull --cache-from "$CI_REGISTRY_IMAGE:latest"
                          --label "org.opencontainers.image.title=.NET Runtimes"
                          --label "org.opencontainers.image.url=$CI_PROJECT_URL"
                          --label "org.opencontainers.image.version=$( cat version.txt )"
                          --label "org.opencontainers.image.revision=$CI_COMMIT_SHA"
                          --label "org.opencontainers.image.created=$CI_JOB_STARTED_AT"
                          --push
                          --platform "$PLATFORMS"
                          --tag $PIPELINE_IMAGE_TAG
                          .

# Update "latest" tag for main branch builds only.
docker-push-gitlab-latest:
  extends: .docker
  stage: push
  only:
    - main
  variables:
    # Docker-only operation, skip Git fetch.
    GIT_STRATEGY: none
  script:
    - skopeo copy -a "docker://$PIPELINE_IMAGE_TAG" "docker://$CI_REGISTRY_IMAGE:latest"

# Set version tags based on the .NET runtime version.
docker-push-gitlab-tags:
  extends: .docker
  stage: push
  only:
    - main
    - branches
  variables:
    # Docker-only operation, skip Git fetch.
    GIT_STRATEGY: none
  script:
    - skopeo copy -a "docker://$PIPELINE_IMAGE_TAG" "docker://$CI_REGISTRY_IMAGE:$( cat version.txt )"
    # Also tag short version if no runtime is a re-release.
    - test -s version-short.txt &&
      skopeo copy -a "docker://$PIPELINE_IMAGE_TAG" "docker://$CI_REGISTRY_IMAGE:$( cat version-short.txt )"

# Validate that all runtimes are found by the dotnet utility.
dotnet-check-runtimes:
  stage: test
  image: $PIPELINE_IMAGE_TAG
  script:
    - ./dotnet-check-runtimes.sh